/*
 * This class is used to convert IP Address with HEX format
 * that extracted from the pay load of data packet
 * to normal IP Address with decimal and dot format
 */
package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * 
 * @author Yuesheng Zhong @ IDA
 * 
 */
public class IPAddressUtil {
	/**
	 * convert IP address with hex format to normal decimal format with dot in between
	 * @param hexIPAddress
	 * @return
	 */
	public static String convertIPAddressFromHexToDecimal(String hexIPAddress) {
		StringBuilder sb = new StringBuilder();
		
		sb.append(hex2decimal(hexIPAddress.substring(0,2)));
		sb.append(".");
		sb.append(hex2decimal(hexIPAddress.substring(2,4)));
		sb.append(".");
		sb.append(hex2decimal(hexIPAddress.substring(4,6)));
		sb.append(".");
		sb.append(hex2decimal(hexIPAddress.substring(6)));
		
		return sb.toString();
	}

	/**
	 * convert hex to decimal
	 * @param hex
	 * @return
	 */
	private static long hex2decimal(String hex) {
		long result = 0;
		int length = hex.length();

		for (int i = 1, index = 0; i < length; i++, index++) {
			result += ((1 << (4 * (length - i))))
					* hex2decimalWith1Digit(hex.substring(index, i));
		}

		result += hex2decimalWith1Digit(hex.substring(length - 1));

		return result;
	}

	/**
	 * convert one digit hex to decimal
	 * @param hex
	 * @return
	 */
	private static int hex2decimalWith1Digit(String hex) {
		hex = hex.toUpperCase();
		switch (hex) {
		case "A":
			return 10;
		case "B":
			return 11;
		case "C":
			return 12;
		case "D":
			return 13;
		case "E":
			return 14;
		case "F":
			return 15;
		default:
			return Integer.parseInt(hex);
		}
	}
	
	public static void main(String[] args){
		System.out.println((short)0x0800);
	}
	
}
