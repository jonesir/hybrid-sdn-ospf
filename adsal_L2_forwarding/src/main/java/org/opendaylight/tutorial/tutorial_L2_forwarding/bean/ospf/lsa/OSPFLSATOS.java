package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class OSPFLSATOS {
	public static final short LENGTH = 4;
	private byte tos;
	private byte extra;
	private short tosMetric;
	
	public OSPFLSATOS() {
		this.tos = 0;
		this.extra = 0;
		this.tosMetric = 0;
	}

	public byte getTos() {
		return tos;
	}

	public void setTos(byte tos) {
		this.tos = tos;
	}

	public byte getExtra() {
		return extra;
	}

	public void setExtra(byte extra) {
		this.extra = extra;
	}

	public short getTosMetric() {
		return tosMetric;
	}

	public void setTosMetric(short tosMetric) {
		this.tosMetric = tosMetric;
	}
	
	public byte[] toBytes() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(this.tos).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(this.extra).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(this.tosMetric).array());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
