package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

public class OSPFLLSDB {
	public static final int LENGTH = 12;
	private byte[] llsdb = new byte[]{(byte)0xff ,(byte)0xf6 ,(byte)0x00 ,(byte)0x03 ,(byte)0x00 ,(byte)0x01 ,(byte)0x00 ,(byte)0x04 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x01};
	
	public byte[] toByteArray() {
		return this.llsdb;
	}
}