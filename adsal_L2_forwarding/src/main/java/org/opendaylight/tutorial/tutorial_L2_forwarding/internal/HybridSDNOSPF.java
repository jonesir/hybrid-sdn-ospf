/*
 * Copyright (C) 2014 SDN Hub

 Licensed under the GNU GENERAL PUBLIC LICENSE, Version 3.
 You may not use this file except in compliance with this License.
 You may obtain a copy of the License at

    http://www.gnu.org/licenses/gpl-3.0.txt

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 implied.

 *
 */

package org.opendaylight.tutorial.tutorial_L2_forwarding.internal;

import java.util.Set;
import java.lang.String;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Map;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;
import org.opendaylight.controller.sal.core.ConstructionException;
import org.opendaylight.controller.sal.core.Node;
import org.opendaylight.controller.sal.core.NodeConnector;
import org.opendaylight.controller.sal.flowprogrammer.IFlowProgrammerService;
import org.opendaylight.controller.sal.packet.Ethernet;
import org.opendaylight.controller.sal.packet.IDataPacketService;
import org.opendaylight.controller.sal.packet.IListenDataPacket;
import org.opendaylight.controller.sal.packet.IPv4;
import org.opendaylight.controller.sal.packet.Packet;
import org.opendaylight.controller.sal.packet.PacketException;
import org.opendaylight.controller.sal.packet.PacketResult;
import org.opendaylight.controller.sal.packet.RawPacket;
import org.opendaylight.controller.sal.utils.EtherTypes;
import org.opendaylight.controller.sal.utils.IPProtocols;
import org.opendaylight.controller.sal.utils.NetUtils;
import org.opendaylight.controller.switchmanager.ISwitchManager;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFDatabaseDescription;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFHeader;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFHello;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFType;
import org.opendaylight.tutorial.tutorial_L2_forwarding.util.DataPacketAssembler;
import org.opendaylight.tutorial.tutorial_L2_forwarding.util.OSPFLogger;
import org.opendaylight.tutorial.tutorial_L2_forwarding.util.RawDumper;

public class HybridSDNOSPF implements IListenDataPacket {
    private static final Logger logger = LoggerFactory
            .getLogger(HybridSDNOSPF.class);
    private ISwitchManager switchManager = null;
    private IFlowProgrammerService programmer = null;
    private IDataPacketService dataPacketService = null;
    private static final String ETHERNET = "Ethernet";
    
    private NodeConnector backup_connector ;
    private static final String BACKUP_CONNECTOR_ID = "8";
    private boolean gotBackupConnector = false;
    private Map<String, NodeConnector> ip_to_port = new HashMap<String, NodeConnector>();
    private Map<String, byte[]> neighbors = new HashMap<String, byte[]>();
    private boolean transparent = false;

    void setDataPacketService(IDataPacketService s) {
        this.dataPacketService = s;
    }

    void unsetDataPacketService(IDataPacketService s) {
        if (this.dataPacketService == s) {
            this.dataPacketService = null;
        }
    }

    public void setFlowProgrammerService(IFlowProgrammerService s)
    {
        this.programmer = s;
    }

    public void unsetFlowProgrammerService(IFlowProgrammerService s) {
        if (this.programmer == s) {
            this.programmer = null;
        }
    }

    void setSwitchManager(ISwitchManager s) {
        logger.debug("SwitchManager set");
        this.switchManager = s;
    }

    void unsetSwitchManager(ISwitchManager s) {
        if (this.switchManager == s) {
            logger.debug("SwitchManager removed!");
            this.switchManager = null;
        }
    }

    /**
     * Function called by the dependency manager when all the required
     * dependencies are satisfied
     *
     */
    void init() {
        logger.info("Initialized");
        // Disabling the SimpleForwarding and ARPHandler bundle to not conflict with this one
        BundleContext bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
        for(Bundle bundle : bundleContext.getBundles()) {
            if (bundle.getSymbolicName().contains("simpleforwarding")) {
                try {
                    bundle.uninstall();
                } catch (BundleException e) {
                    logger.error("Exception in Bundle uninstall "+bundle.getSymbolicName(), e); 
                }   
            }   
        }   
 
    }

    /**
     * Function called by the dependency manager when at least one
     * dependency become unsatisfied or when the component is shutting
     * down because for example bundle is being stopped.
     *
     */
    void destroy() {
    }

    /**
     * Function called by dependency manager after "init ()" is called
     * and after the services provided by the class are registered in
     * the service registry
     *
     */
    void start() {
        logger.info("Started");
    }

    /**
     * Function called by the dependency manager before the services
     * exported by the component are unregistered, this will be
     * followed by a "destroy ()" calls
     *
     */
    void stop() {
        logger.info("Stopped");
    }

    private void floodPacket(RawPacket inPkt) {
        NodeConnector incoming_connector = inPkt.getIncomingNodeConnector();
        // this is the open flow switch that sends this data packet
        Node incoming_node = incoming_connector.getNode();

        // get all connected and active node connectors 
        Set<NodeConnector> nodeConnectors =
                this.switchManager.getUpNodeConnectors(incoming_node);

        for (NodeConnector p : nodeConnectors) {
        	// flood all connected routers except the one sending this data packet
            if (!p.equals(incoming_connector)) {
                try {
                    RawPacket destPkt = new RawPacket(inPkt);
                    destPkt.setOutgoingNodeConnector(p);
                    this.dataPacketService.transmitDataPacket(destPkt);
                } catch (ConstructionException e2) {
                    continue;
                }
            }
        }
    }

    @Override
    public PacketResult receiveDataPacket(RawPacket inPkt) {
        if (inPkt == null) {
            return PacketResult.IGNORED;
        }
        
        if (transparent)
        	processDataPacket(inPkt);
        else
        	handelDataPacket(inPkt);
        
        Packet formattedPak = this.dataPacketService.decodeDataPacket(inPkt);
        
        if (!(formattedPak instanceof Ethernet)) {
            return PacketResult.IGNORED;
        }
        return PacketResult.CONSUME;
    }
    
    private void processDataPacket(RawPacket inPkt) {
    	byte[] dataPacket = inPkt.getPacketData();
    	System.out.println("********************************************************************************");
    	System.out.println("********************************************************************************");
    	/** decoded OpenFlow packet */
    	Packet formattedPkt = this.dataPacketService.decodeDataPacket(inPkt);
    	
    	/** Interface of OpenFlow Switch */
    	NodeConnector incoming_connector = inPkt.getIncomingNodeConnector();
    	String interfaceNumber = incoming_connector.getNodeConnectorIDString();
    	Set<NodeConnector> upConnectors = this.switchManager.getUpNodeConnectors(incoming_connector.getNode());
    	String ncID;
    	
    	/** print out every connected and up connector */
    	System.out.println("--- port and type --- ");
    	for (NodeConnector nc : upConnectors) {
    		ncID = nc.getNodeConnectorIDString();
    		System.out.println("|||||||||||   " + ncID + "   -----> " + (((ncID.equals(BACKUP_CONNECTOR_ID))?"[[[[[[[[[[[[[ Backup Router ]]]]]]]]]]]]]]]" : "((( Normal Router )))") + ""));
    	}
    	
    	/** print out all known ip_to_port relationship */
    	System.out.println("\n--- ip to port relationship -----");
    	for (String ipString : ip_to_port.keySet())
    		System.out.println(ipString + " -> " + ip_to_port.get(ipString).getNodeConnectorIDString());
    	
    	/** ETHERNET */
        if(isEthernet(formattedPkt)) {
        	/** IPv4 */
        	if (isIPPacket((Ethernet)formattedPkt)) {
        		IPv4 ipPak = (IPv4) ((Ethernet)formattedPkt).getPayload();
//        		System.out.println("\nIPv4             : " + ipPak);
        		int sipAddr = ipPak.getSourceAddress();
        		InetAddress sip = NetUtils.getInetAddress(sipAddr);
        		int dipAddr = ipPak.getDestinationAddress();
        		InetAddress dip = NetUtils.getInetAddress(dipAddr);
        		System.out.println("\nprotocol         : " + ipPak.getProtocol() + " ------> " + IPProtocols.getProtocolName(ipPak.getProtocol()));
        		System.out.println("\n" + sip.toString().substring(1) + " ====> " + dip.toString().substring(1) + " on connector " + interfaceNumber);
        		
        		/** OSPF, Only at this point, do the forwarding action */
        		if (isOSPFMessage(ipPak)){
        			/** type of OSPF message */
					int type = dataPacket[35];
					System.out.println("\n\ntype of ospf :  " + type + "\n\n");
					switch (type) {
					case OSPFType.HELLO: 
						OSPFLogger.logHello();
						break;
					case OSPFType.DATABASE_DESCRIPTION:
						OSPFLogger.logDataDescription();
						break;
					case OSPFType.LINK_STATE_REQUEST:
						OSPFLogger.logLinkStateRequest();
						break;
					case OSPFType.LINK_STATE_UPDATE:
						OSPFLogger.logLinkStateUpdate();
						break;
					case OSPFType.LINK_STATE_ACKNOWLEDGMENT:
						OSPFLogger.logLinkStateAcknowledge();
						break;
					default:
						break;
					}
					
        			System.out.println("\nPACKET LENGTH ==> " + dataPacket.length);
					// print raw data packet in hex format like it would look like in wireshark
					ByteBuffer buffer = ByteBuffer.wrap(dataPacket);			
					buffer.order(ByteOrder.BIG_ENDIAN);
					RawDumper.logRawDataPacket(dataPacket);
					/** if the packet comes from backup router, we need to flood all other connected routers  */
					if (interfaceNumber.equals(BACKUP_CONNECTOR_ID)) {
						/** save the incoming connector as backup connector if it is sending for the first time */
						if(!gotBackupConnector){
							backup_connector = incoming_connector;
							gotBackupConnector = true;
						}
						
						/** backup router want to sent OSPF message to a particular router */
						if (ip_to_port.containsKey(dip.toString()))
							sendOSPF2Router(inPkt, ip_to_port.get(dip.toString()));
						else/** in this case, the destination should be OSPF broadcast address /224.0.0.5, then, flood it */
							floodPacket(inPkt);
						
					} else {// data packet sent from routers other than backup router
						/** save the IP address and corresponding incoming connector to DB for later reference */
						ip_to_port.put(sip.toString(), incoming_connector);
						
						/** send data packet to backup router if any */
						if (gotBackupConnector) {
							try {
								RawPacket destPkt = new RawPacket(inPkt);
								destPkt.setOutgoingNodeConnector(backup_connector);
								this.dataPacketService.transmitDataPacket(destPkt);
							} catch (ConstructionException e) {
								e.printStackTrace();
							}
						}
						
						/** otherwise do nothing */
					}
        		}
        	}
        }
    	
    }
    
    private void sendOSPF2Router(RawPacket dataPacket, NodeConnector destination_connector) {
    	try {
			RawPacket destPkt = new RawPacket(dataPacket);
			destPkt.setOutgoingNodeConnector(destination_connector);
			this.dataPacketService.transmitDataPacket(destPkt);
		} catch (ConstructionException e) {
			e.printStackTrace();
		}
    }
    
    private void handelDataPacket(RawPacket inPkt) {
    	/** raw data packet that received by OpenFlow Switch from wire in byte array */
    	byte[] dataPacket = inPkt.getPacketData();
    	System.out.println("********************************************************************************");
    	System.out.println("********************************************************************************");
    	
    	/** decoded OpenFlow packet */
    	Packet formattedPkt = this.dataPacketService.decodeDataPacket(inPkt);
    	
    	/** Interface of OpenFlow Switch */
    	NodeConnector incoming_connector = inPkt.getIncomingNodeConnector();
    	String interfaceNumber = incoming_connector.getNodeConnectorIDString();
    	
        /** ETHERNET */
        if(isEthernet(formattedPkt)) {
        	/** IPv4 */
        	if (isIPPacket((Ethernet)formattedPkt)) {
        		IPv4 inIPv4 = (IPv4) ((Ethernet)formattedPkt).getPayload();
        		int sipAddr = inIPv4.getSourceAddress();
        		InetAddress sip = NetUtils.getInetAddress(sipAddr);
        		int dipAddr = inIPv4.getDestinationAddress();
        		InetAddress dip = NetUtils.getInetAddress(dipAddr);
        		System.out.println("\nprotocol         : " + inIPv4.getProtocol() + " ------> " + IPProtocols.getProtocolName(inIPv4.getProtocol()));
        		System.out.println("\n" + sip + " ====> " + dip + " on connector " + interfaceNumber);
        		
        		/** OSPF */
        		if (isOSPFMessage(inIPv4)){
        			System.out.println("\n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<< INCOMING OSPF PACKET");
        			System.out.println("\nPACKET LENGTH ==> " + dataPacket.length);
					// print raw data packet in hex format like it would in wireshark
					ByteBuffer buffer = ByteBuffer.wrap(dataPacket);			
					buffer.order(ByteOrder.BIG_ENDIAN);
					RawDumper.logRawDataPacket(dataPacket);
					
					/** type of OSPF message */
					int type = dataPacket[35];
					System.out.println("\n\ntype of ospf :  " + type + "\n");
					byte[] response = null ;
					switch (type) {
					case OSPFType.HELLO: 
						OSPFLogger.logHello();
						response = handleOSPFHelloMessage(inIPv4, dataPacket, incoming_connector);
						break;
					case OSPFType.DATABASE_DESCRIPTION:
						OSPFLogger.logDataDescription();
						handleOSPFDatabaseDescriptionMessage(inIPv4, dataPacket,incoming_connector);
						break;
					case OSPFType.LINK_STATE_REQUEST:
						OSPFLogger.logLinkStateRequest();
						handleOSPFLinkStateRequestMessage(inIPv4, dataPacket,incoming_connector);
						break;
					case OSPFType.LINK_STATE_UPDATE:
						OSPFLogger.logLinkStateUpdate();
						handleOSPFLinkStateUpdateMessage(inIPv4, dataPacket,incoming_connector);
						break;
					case OSPFType.LINK_STATE_ACKNOWLEDGMENT:
						OSPFLogger.logLinkStateAcknowledge();
						handleOSPFLinkStateAcknowledgment(inIPv4, dataPacket, incoming_connector);
						break;
					default:
						break;
					}
					
					if (null != response)
						respond(response, incoming_connector);
        		}
        	}
        }
    }
    
    private void respond(byte[] response, NodeConnector outgoing_connector) {
    	RawPacket r;
		try {
			r = new RawPacket(response);
			r.setOutgoingNodeConnector(outgoing_connector);
			System.out.println("\n>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> OUTGOING OSPF PACKET");
			System.out.println("OSPF Response Length : " + response.length);
			RawDumper.logRawDataPacket(response);
			this.dataPacketService.transmitDataPacket(r);
		} catch (ConstructionException e) {
			e.printStackTrace();
		}
    }
    
    /** takes care of OSPF hello message */
    private byte[] handleOSPFHelloMessage(IPv4 inIPv4, byte[] dataPacket, NodeConnector incoming_connector) {
//    	byte[] ospfPacket = DataPacketAssembler.assembleOSPFHelloPacket(dataPacket, neighbors);
    	/** create and configure OSPF part */
    	byte[] ospfByteArray;
    	OSPFHello inHello = DataPacketAssembler.decodeOSPFHello(dataPacket);
    	OSPFHeader inHeader = inHello.getHeader();
    	
    	/** create and configure outgoing ethernet part */
    	Ethernet inEthernet = (Ethernet)inIPv4.getParent();
    	Ethernet outEthernet = new Ethernet();
    	outEthernet.setSourceMACAddress(DataPacketAssembler.SOURCE_MAC_ADDRESS_BYTES);// source is the self-MAC-address
    	outEthernet.setEtherType(inEthernet.getEtherType());// ethernet type stays unchanged
    	
    	/** create and configure outgoing ipv4 part */
    	IPv4 outIPv4 = new IPv4();// when creating an new IPv4 packet, identification field will be created automatically
    	try {
    		// source IP is the self IP
			outIPv4.setSourceAddress(InetAddress.getByAddress(DataPacketAssembler.SOURCE_IP_ADDRESS_BYTES));
			outIPv4.setDiffServ(inIPv4.getDiffServ());
			outIPv4.setECN(inIPv4.getECN());
			outIPv4.setFlags(inIPv4.getFlags());
			outIPv4.setFragmentOffset(inIPv4.getFragmentOffset());
			outIPv4.setOptions(inIPv4.getOptions());
			outIPv4.setTtl(inIPv4.getTtl());
			outIPv4.setProtocol(inIPv4.getProtocol());
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
    	
    	// if router ID included in the incoming hello message's neighbor field, response with DBD message
    	if (inHello.getNeighbors().contains(ByteBuffer.wrap(DataPacketAssembler.ROUTER_ID_BYTES).getInt())){
    		// response with DBD message and make it to init, more and master
    		OSPFDatabaseDescription db = new OSPFDatabaseDescription(0,0,1,1,1,null);
    		// set router ID in header
    		db.getHeader().setRouterID(ByteBuffer.wrap(DataPacketAssembler.ROUTER_ID_BYTES).getInt());
    		ospfByteArray = db.toByteArray();
    		
    		/** Ethernet part */
    		// destination is the source of incoming hello message
    		outEthernet.setDestinationMACAddress(inEthernet.getSourceMACAddress());
    		
    		/** IP part */
    		// destination IP address the source IP address of incoming packet
    		outIPv4.setDestinationAddress(inIPv4.getSourceAddress());
    	} else {// otherwise response with OSPF Hello message
    		
    		OSPFHello outHello = new OSPFHello(0,0,0,0,null);
    		outHello.setDesignatedRouter(ByteBuffer.wrap(DataPacketAssembler.SOURCE_IP_ADDRESS_BYTES).getInt());
    		// set neighbor router ID of outgoing hello packet to the Router ID of incoming OSPF packet
    		outHello.addNeighbor(inHeader.getRouterID());
    		// set Router ID of outgoing hello packet to self defined ID
    		outHello.getHeader().setRouterID(ByteBuffer.wrap(DataPacketAssembler.ROUTER_ID_BYTES).getInt());
    		ospfByteArray = outHello.toByteArray();
    		
    		/** Ethernet part */
    		// destination if the OSPF broadcast MAC address same as in the incoming packet
    		outEthernet.setDestinationMACAddress(inEthernet.getDestinationMACAddress());
    		
    		try {
    			/** IP part */
    			outIPv4.setDestinationAddress(InetAddress.getByAddress(DataPacketAssembler.BROADCAST_IP_ADDRESS_BYTES));
    		} catch (UnknownHostException e3) {
    			e3.printStackTrace();
    		}
    		
    	}
    	
    	// append OSPF Hello packet to IPv4 packet
    	outIPv4.setRawPayload(ospfByteArray);
    	
    	// append IPv4 packet to Ethernet packet
    	outEthernet.setPayload(outIPv4);
    	
    	try {
			return outEthernet.serialize();
		} catch (PacketException e) {
			e.printStackTrace();
		}
		
///** FLIP L2 & L3 ADDRESSES: IP Header Checksum will be computed and set automatically by framework */
//		try {
//			Packet p = this.dataPacketService.decodeDataPacket(new RawPacket(ospfPacket));
//			
//			Ethernet e = (Ethernet)p;
//			e.setSourceMACAddress(DataPacketAssembler.SOURCE_MAC_ADDRESS_BYTES);
//			e.setDestinationMACAddress(DataPacketHelper.createByteArray(dataPacket, DataPacketAssembler.ETHERNET_DESTINATION_OFFSET, 6));
//			
//			/** ===== prepare for the final data packet to be sent to OpenFlow switch from controller ===== */
//			// switch Layer 3 source and destination address
//			inIPv4 = (IPv4)e.getPayload();
//			inIPv4.setIdentification(DataPacketAssembler.IP_IDENTIFICATION++);
//			inIPv4.setSourceAddress(InetAddress.getByAddress(DataPacketAssembler.SOURCE_IP_ADDRESS_BYTES));
//			inIPv4.setDestinationAddress(InetAddress.getByAddress(DataPacketAssembler.IP_BROADCAST_ADDRESS_BYTES));
//			
//			/** ===== send OSPF hello response to OpenFlow Switch, where it will be forwarded to OSPF router ===== */  
//			return e.serialize();
//			
//		} catch (PacketException e2) {
//			e2.printStackTrace();
//		} catch (ConstructionException e1) {
//			e1.printStackTrace();
//		} catch (UnknownHostException e1) {
//			e1.printStackTrace();
//		}
		
		return null;
    }
    
    private byte[] handleOSPFDatabaseDescriptionMessage(IPv4 inIPv4, byte[] dataPacket, NodeConnector incoming_connector){
    	return null;
    }
    
    private byte[] handleOSPFLinkStateRequestMessage(IPv4 inIPv4, byte[] dataPacket, NodeConnector incoming_connector) {
    	return null;
    }
    
    private byte[] handleOSPFLinkStateUpdateMessage(IPv4 inIPv4, byte[] dataPacket, NodeConnector incoming_connector) {
    	return null;
    }
    
    private byte[] handleOSPFLinkStateAcknowledgment(IPv4 inIPv4, byte[] dataPacket, NodeConnector incoming_connector) {
    	return null;
    }
    
    private boolean isIPPacket(Ethernet e) {
    	return e.getEtherType()==EtherTypes.IPv4.shortValue();
    }

    /** check if IP packet is OSPF packet */
    private boolean isOSPFMessage(IPv4 ipPak) {
    	return (IPProtocols.OSPFIGP.byteValue() == ipPak.getProtocol());
    }
    
    /** check if incoming packet is Ethernet packet */
    private boolean isEthernet(Packet packet) {
    	return ETHERNET.equalsIgnoreCase(packet.getClass().getSimpleName());
    }
    
}
