package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class InternetChecksum {

	  /**
	   * Calculate the Internet Checksum of a buffer (RFC 1071 - http://www.faqs.org/rfcs/rfc1071.html)
	   * Algorithm is
	   * 1) apply a 16-bit 1's complement sum over all octets (adjacent 8-bit pairs [A,B], final odd length is [A,0])
	   * 2) apply 1's complement to this final sum
	   *
	   * Notes:
	   * 1's complement is bitwise NOT of positive value.
	   * Ensure that any carry bits are added back to avoid off-by-one errors
	   *
	   *
	   * @param buf The message
	   * @return The checksum
	   */
	  public static long calculateChecksum(byte[] buf) {
	    int length = buf.length;
	    int i = 0;

	    long sum = 0;
	    long data;

	    // Handle all pairs
	    while (length > 1) {
	      // Corrected to include @Andy's edits and various comments on Stack Overflow
	      data = (((buf[i] << 8) & 0xFF00) | ((buf[i + 1]) & 0xFF));
	      sum += data;
	      // 1's complement carry bit correction in 16-bits (detecting sign extension)
	      if ((sum & 0xFFFF0000) > 0) {
	        sum = sum & 0xFFFF;
	        sum += 1;
	      }

	      i += 2;
	      length -= 2;
	    }

	    // Handle remaining byte in odd length buffers
	    if (length > 0) {
	      // Corrected to include @Andy's edits and various comments on Stack Overflow
	      sum += (buf[i] << 8 & 0xFF00);
	      // 1's complement carry bit correction in 16-bits (detecting sign extension)
	      if ((sum & 0xFFFF0000) > 0) {
	        sum = sum & 0xFFFF;
	        sum += 1;
	      }
	    }

	    // Final 1's complement value correction to 16-bits
	    sum = ~sum;
	    sum = sum & 0xFFFF;
	    return sum;

	  }
	  
	  public static long checksum(byte[] buf, int length) {
		    int i = 0;
		    long sum = 0;
		    while (length > 0) {
		        sum += (buf[i++]&0xff) << 8;
		        if ((--length)==0) break;
		        sum += (buf[i++]&0xff);
		        --length;
		    }

		    return (~((sum & 0xFFFF)+(sum >> 16)))&0xFFFF;
		}
	  
	  public static String byte2hex(byte b) {
	    	String result = Integer.toHexString((int)(b&0xff));
	    	return result.length()==1 ? "0" + result : result; 
	    }
	  
	  public static void main(String[] args) {
		  byte[] sample = new byte[] {(byte)0x02 ,(byte)0x01 ,(byte)0x00 ,(byte)0x30 ,(byte)0x0a ,(byte)0x0b ,(byte)0x0c ,(byte)0x0d ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 
				  ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0xff ,(byte)0xff ,(byte)0xff ,(byte)0x00 ,(byte)0x00 ,(byte)0x0a 
				  ,(byte)0x12 ,(byte)0x01 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x28 ,(byte)0x0a ,(byte)0x00 ,(byte)0x00 ,(byte)0x03 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x00 ,(byte)0x01 ,(byte)0x01 
				  ,(byte)0x01 ,(byte)0x01 };
		  byte[] sampleOSPF = new byte[]{(byte)0x01, (byte)0x00, (byte)0x5e , (byte)0x00 , (byte)0x00 , (byte)0x05 , (byte)0x00 , (byte)0x1d , (byte)0xa1 , (byte)0x89 , (byte)0x85 , (byte)0xc0 , (byte)0x08 , (byte)0x00 , (byte)0x45 , (byte)0xc0 
				  , (byte)0x00 , (byte)0x4c , (byte)0x00 , (byte)0xdf , (byte)0x00 , (byte)0x00 , (byte)0x01 , (byte)0x59 , (byte)0xcb , (byte)0xb4 , (byte)0x0a , (byte)0x00 , (byte)0x02 , (byte)0x01 , (byte)0xe0 , (byte)0x00 
				  , (byte)0x00 , (byte)0x05 , (byte)0x02 , (byte)0x01 , (byte)0x00 , (byte)0x2c , (byte)0x01 , (byte)0x01 , (byte)0x01 , (byte)0x01 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0xde , (byte)0x9b 
				  , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0xff , (byte)0xff , (byte)0xff , (byte)0x00 , (byte)0x00 , (byte)0x0a 
				  , (byte)0x12 , (byte)0x01 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x28 , (byte)0x0a , (byte)0x00 , (byte)0x02 , (byte)0x01 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0xff , (byte)0xf6 
				  , (byte)0x00 , (byte)0x03 , (byte)0x00 , (byte)0x01 , (byte)0x00 , (byte)0x04 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x01};
		  byte[] sampleIP = new byte[] {
				  (byte)0x45 , (byte)0xc0 
				  , (byte)0x00 , (byte)0x50 , (byte)0x03 , (byte)0xe8 , (byte)0x00 , (byte)0x00 , (byte)0x01 , (byte)0x59 , (byte)0xa0 , (byte)0xaa , (byte)0x0a , (byte)0x00 , (byte)0x00 , (byte)0x03 , (byte)0x0a , (byte)0x00 
				  , (byte)0x00 , (byte)0x01 , (byte)0x02 , (byte)0x01 , (byte)0x00 , (byte)0x30 , (byte)0x0a , (byte)0x0b , (byte)0x0c , (byte)0x0d , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0xca , (byte)0x7d 
				  , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0xff , (byte)0xff , (byte)0xff , (byte)0x00 , (byte)0x00 , (byte)0x0a 
				  , (byte)0x12 , (byte)0x01 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x28 , (byte)0x0a , (byte)0x00 , (byte)0x00 , (byte)0x03 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x01 , (byte)0x01 
				  , (byte)0x01 , (byte)0x01 , (byte)0xff , (byte)0xf6 , (byte)0x00 , (byte)0x03 , (byte)0x00 , (byte)0x01 , (byte)0x00 , (byte)0x04 , (byte)0x00 , (byte)0x00 , (byte)0x00 , (byte)0x01 };
		  long checksum = checksum(sampleIP,sampleIP.length);
		  
		  System.out.println(checksum + " ==> " + Long.toHexString(checksum));
		  
//		  List<Byte> l = new ArrayList<Byte>();
//		  l.add((byte) 0x00);
//		  l.add((byte) 0x1d);
//		  l.add((byte) 0x46);
//		  l.add((byte) 0xdd);
//		  l.add((byte) 0x4c);
//		  l.add((byte) 0x89);
//		  
//		  Byte[] b = l.toArray(new Byte[0]);
//		  for (byte i : b) {
//			  System.out.println(Integer.toHexString((int)(i&0xff)));
//		  }
	  }
	  
	}