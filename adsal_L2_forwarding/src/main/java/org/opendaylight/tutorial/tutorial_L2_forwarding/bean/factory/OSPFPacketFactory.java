package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.factory;

import java.util.List;

import org.opendaylight.controller.sal.packet.Ethernet;
import org.opendaylight.controller.sal.packet.IPv4;
import org.opendaylight.controller.sal.utils.EtherTypes;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPF;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFDatabaseDescription;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFHello;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFLinkStateAcknowledgment;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFLinkStateRequest;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFLinkStateRequest.LinkState;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFLinkStateUpdate;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa.OSPFLSA;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa.OSPFLSAHeader;

/**
 * Factory class to create OSPF packets
 * 
 * @author Yuesheng Zhong
 * 
 */
public class OSPFPacketFactory {
	public static Ethernet createOSPFHelloPacket(int routerID, int areaID,
			int dr, int bdr, List<Integer> neighbors) {
		OSPF ospf = new OSPFHello(routerID, areaID, dr, bdr, neighbors);
		return getEthernet(ospf);
	}

	public static Ethernet createOSPFDatabaseDescriptionPacket(int routerID,
			int areaID, int init, int more, int master,
			List<OSPFLSAHeader> lsaHeaders) {
		OSPF ospf = new OSPFDatabaseDescription(routerID, areaID, init, more,
				master, lsaHeaders);
		return getEthernet(ospf);
	}

	public static Ethernet createOSPFLinkStateRequestPacket(int routerID,
			int areaID, List<LinkState> lss) {
		OSPF ospf = new OSPFLinkStateRequest(routerID, areaID, lss);
		return getEthernet(ospf);
	}

	public static Ethernet createOSPFLinkStateUpdatePacket(int routerID,
			int areaID, List<OSPFLSA> lsas) {
		OSPF ospf = new OSPFLinkStateUpdate(routerID, areaID, lsas);
		return getEthernet(ospf);
	}

	public static Ethernet createOSPFLinkStateAcknowledgmentPacket(
			int routerID, int areaID, List<OSPFLSAHeader> lsaHeaders) {
		OSPF ospf = new OSPFLinkStateAcknowledgment(routerID, areaID,
				lsaHeaders);
		return getEthernet(ospf);
	}

	private static Ethernet getEthernet(OSPF ospf) {
		/* create ethernet packet type set */
		Ethernet ospfEthernetPacket = new Ethernet();
		ospfEthernetPacket.setEtherType(EtherTypes.IPv4.shortValue());

		/* embed empty IPv4 into ethernet */
		IPv4 ospfIPv4Packet = new IPv4();
		ospfIPv4Packet.setRawPayload(ospf.toByteArray());
		ospfEthernetPacket.setPayload(ospfIPv4Packet);
		return ospfEthernetPacket;
	}
}
