package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*    
RFC 2328
A.4.5 AS-external-LSAs

    AS-external-LSAs are the Type 5 LSAs.  These LSAs are originated by
    AS boundary routers, and describe destinations external to the AS.
    For details concerning the construction of AS-external-LSAs, see
    Section 12.4.3.

    AS-external-LSAs usually describe a particular external destination.
    For these LSAs the Link State ID field specifies an IP network
    number (if necessary, the Link State ID can also have one or more of
    the network's "host" bits set; see Appendix E for details).  AS-
    external-LSAs are also used to describe a default route.  Default
    routes are used when no specific route exists to the destination.
    When describing a default route, the Link State ID is always set to
    DefaultDestination (0.0.0.0) and the Network Mask is set to 0.0.0.0.

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |            LS age             |     Options   |      5        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                        Link State ID                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     Advertising Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     LS sequence number                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         LS checksum           |             length            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                         Network Mask                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |E|     0       |                  metric                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                      Forwarding address                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                      External Route Tag                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |E|    TOS      |                TOS  metric                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                      Forwarding address                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                      External Route Tag                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |
*/
public class OSPFLSAASExternal extends OSPFLSA {
	private static final short DEFAULT_LENGTH = 28;
	private int networkMask;
	private byte extra1;
	private int metric;
	private int forwardingAddress1;
	private int externalRouteTag1;
	private byte extra2;
	private int tosMetric;
	private int forwardingAddress2;
	private int externalRouteTag2;
	
	public OSPFLSAASExternal() {
		lsaHeader = new OSPFLSAHeader((byte)0x05);
		networkMask = 0;
		extra1 = 0;
		metric = 0;
		forwardingAddress1 = 0;
		externalRouteTag1 = 0;
		extra2 = 0;
		tosMetric = 0;
		forwardingAddress2 = 0;
		externalRouteTag2 = 0;
	}
	
	public int getNetworkMask() {
		return networkMask;
	}

	public void setNetworkMask(int networkMask) {
		this.networkMask = networkMask;
	}

	public byte getExtra1() {
		return extra1;
	}

	public void setExtra1(byte extra1) {
		this.extra1 = extra1;
	}

	public int getMetric() {
		return metric;
	}

	public void setMetric(int metric) {
		this.metric = metric;
	}

	public int getForwardingAddress1() {
		return forwardingAddress1;
	}

	public void setForwardingAddress1(int forwardingAddress1) {
		this.forwardingAddress1 = forwardingAddress1;
	}

	public int getExternalRouteTag1() {
		return externalRouteTag1;
	}

	public void setExternalRouteTag1(int externalRouteTag1) {
		this.externalRouteTag1 = externalRouteTag1;
	}

	public byte getExtra2() {
		return extra2;
	}

	public void setExtra2(byte extra2) {
		this.extra2 = extra2;
	}

	public int getTosMetric() {
		return tosMetric;
	}

	public void setTosMetric(int tosMetric) {
		this.tosMetric = tosMetric;
	}

	public int getForwardingAddress2() {
		return forwardingAddress2;
	}

	public void setForwardingAddress2(int forwardingAddress2) {
		this.forwardingAddress2 = forwardingAddress2;
	}

	public int getExternalRouteTag2() {
		return externalRouteTag2;
	}

	public void setExternalRouteTag2(int externalRouteTag2) {
		this.externalRouteTag2 = externalRouteTag2;
	}

	@Override
	public byte[] toByteArray() {
		setHeaderChecksumAndLength();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(this.lsaHeader.toByteArray());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(networkMask).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(extra1).array());
			byte[] metricArray = ByteBuffer.allocate(Integer.BYTES).putInt(metric).array();
			outputStream.write(new byte[]{metricArray[1],metricArray[2],metricArray[3]});
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(forwardingAddress1).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(externalRouteTag1).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(extra2).array());
			byte[] tosMetricArray = ByteBuffer.allocate(Integer.BYTES).putInt(tosMetric).array();
			outputStream.write(new byte[]{tosMetricArray[1],tosMetricArray[2],tosMetricArray[3]});
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(forwardingAddress2).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(externalRouteTag2).array());
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setHeaderChecksumAndLength() {
		
	}
	
	@Override
	public short getLength() {
		short length = DEFAULT_LENGTH;
		return length;
	}
}
