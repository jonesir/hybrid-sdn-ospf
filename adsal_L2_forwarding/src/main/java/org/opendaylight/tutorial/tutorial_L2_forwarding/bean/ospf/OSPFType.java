package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

public class OSPFType {
	public static final int HELLO = 1;
	public static final int DATABASE_DESCRIPTION = 2;
	public static final int LINK_STATE_REQUEST = 3;
	public static final int LINK_STATE_UPDATE = 4;
	public static final int LINK_STATE_ACKNOWLEDGMENT = 5;
}
