package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.util.DataPacketHelper;

/**
 * 
 * @author Yuesheng Zhong
 *
 */
/*
   RFC 2328 
   A.3.2 The Hello packet

    Hello packets are OSPF packet type 1.  These packets are sent
    periodically on all interfaces (including virtual links) in order to
    establish and maintain neighbor relationships.  In addition, Hello
    Packets are multicast on those physical networks having a multicast
    or broadcast capability, enabling dynamic discovery of neighboring
    routers.

    All routers connected to a common network must agree on certain
    parameters (Network mask, HelloInterval and RouterDeadInterval).
    These parameters are included in Hello packets, so that differences
    can inhibit the forming of neighbor relationships.  A detailed
    explanation of the receive processing for Hello packets is presented
    in Section 10.5.  The sending of Hello packets is covered in Section
    9.5.


        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |   Version #   |       1       |         Packet length         |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          Router ID                            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                           Area ID                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |           Checksum            |             AuType            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                        Network Mask                           |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         HelloInterval         |    Options    |    Rtr Pri    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     RouterDeadInterval                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                      Designated Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                   Backup Designated Router                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          Neighbor                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |
 */
public class OSPFHello extends OSPF {
	
	private static final short DEFAULT_HELLO_BODY_LENGTH = 20;
	private int networkMask;
	private short helloInterval;
	private byte options;
	private byte priority;
	private int routerDeadInterval;
	private int designatedRouter;
	private int backupDesignatedRouter;
	private List<Integer> neighbors;
	
	public OSPFHello(int routerID, int areaID, int dr, int bdr, List<Integer> neighbors) {
		this.header = new OSPFHeader((byte)0x01);
		this.header.setRouterID(routerID);
		this.header.setAreaID(areaID);
		
		// initialization with default values
		networkMask = -256; 		// 0xffffff00
		helloInterval = 10; 		// 0x000a
		options = 18;				// 0x12
		priority = 1;				// 0x01
		routerDeadInterval = 40; 	// 0x00000028
		designatedRouter = dr;		
		backupDesignatedRouter = bdr;
		this.neighbors = neighbors;
	}
	
	public int getNetworkmask() {
		return networkMask;
	}

	public void setNetworkmask(int networkmask) {
		this.networkMask = networkmask;
	}

	public short getHelloInterval() {
		return helloInterval;
	}

	public void setHelloInterval(short helloInterval) {
		this.helloInterval = helloInterval;
	}

	public byte getOptions() {
		return options;
	}

	public void setOptions(byte options) {
		this.options = options;
	}

	public byte getPriority() {
		return priority;
	}

	public void setPriority(byte priority) {
		this.priority = priority;
	}

	public int getRouterDeadInterval() {
		return routerDeadInterval;
	}

	public void setRouterDeadInterval(int routerDeadInterval) {
		this.routerDeadInterval = routerDeadInterval;
	}

	public int getDesignatedRouter() {
		return designatedRouter;
	}

	public void setDesignatedRouter(int designatedRouter) {
		this.designatedRouter = designatedRouter;
	}

	public int getBackupDesignatedRouter() {
		return backupDesignatedRouter;
	}

	public void setBackupDesignatedRouter(int backupDesignatedRouter) {
		this.backupDesignatedRouter = backupDesignatedRouter;
	}

	public List<Integer> getNeighbors() {
		return neighbors;
	}

	public void setNeighbors(int[] neighbors) {
		for (int neighbor : neighbors)
			this.neighbors.add(neighbor);
	}
	
	public void addNeighbor(int neighbor) {
		this.neighbors.add(neighbor);
	}
	
	public OSPFHeader getHeader() {
		return this.header;
	}

	@Override
	public byte[] toByteArray() {
		calculateAndSetHeaderLengthAndChecksum();
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			// write header first
			outputStream.write(this.header.toByteArray());
			// then hello body
			outputStream.write(getDefaultBodyBytes());
			// finally LLSDB info
			outputStream.write(this.llsdb.toByteArray());
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	/**
	 * calculate and set both length and checksum field in header
	 */
	private void calculateAndSetHeaderLengthAndChecksum() {
		// calculate and set length
		short length = (short)(OSPFHeader.LENGTH + DEFAULT_HELLO_BODY_LENGTH + this.neighbors.size()*4);
		this.header.setLength(length);
		
		// calculate and set checksum
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			// header without authentication fields
			outputStream.write(this.header.toChecksumCalculationByteArray());
			// hello body
			outputStream.write(getDefaultBodyBytes());
			// calculate checksum 
			byte[] checksumBytes = DataPacketHelper.calculateOSPFChecksum(outputStream.toByteArray());
			short[] checksum = new short[checksumBytes.length/2];
			ByteBuffer.wrap(checksumBytes).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(checksum);
			// set checksum
			this.header.setChecksum(checksum[checksum.length-1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * default body fields:
	 * 		network mask, hello interval, options, priority, router dead interval, 
	 * 		designated router, backup designated router, neighbors
	 */
	private byte[] getDefaultBodyBytes() {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(networkMask).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(helloInterval).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(options).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(priority).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(routerDeadInterval).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(designatedRouter).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(backupDesignatedRouter).array());
			for (int neighbor : neighbors)
				outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(neighbor).array());
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
			return new byte[]{0x00, 0x00};
		}
	}
}
