package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;


public abstract class OSPF {
	protected OSPFHeader header;
	protected OSPFLLSDB llsdb = new OSPFLLSDB();
	public abstract byte[] toByteArray();
	public OSPFHeader getHeader() {
		return this.header;
	}
}
