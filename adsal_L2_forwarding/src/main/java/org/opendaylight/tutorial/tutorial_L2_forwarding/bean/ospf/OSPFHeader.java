package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author Yuesheng Zhong
 *
 */
/* 
  RFC 2328 
   A.3.1 The OSPF packet header

    Every OSPF packet starts with a standard 24 byte header.  This
    header contains all the information necessary to determine whether
    the packet should be accepted for further processing.  This
    determination is described in Section 8.2 of the specification.

		0                   1                   2                   3
	    0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |   Version #   |     Type      |         Packet length         |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                          Router ID                            |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                           Area ID                             |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |           Checksum            |             AuType            |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                       Authentication                          |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
	   |                       Authentication                          |
	   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
*/
public class OSPFHeader {
	public static final short LENGTH = 24;
	private byte version;
	private byte type;
	private short length;
	private int routerID;
	private int areaID;
	private short checksum;
	private short authentication;
	private long authenticationData;
	
	/**
	 * fields except source and destination are default, don't need to be modified
	 * @param type
	 */
	public OSPFHeader(byte type) {
		version = 2; 			// 0x02
		this.type = type;
		length = 0;				// 0x0000
		routerID = 0;			// 0x00000000
		areaID = 0;				// 0x00000000
		checksum = 0;			// 0x0000
		authentication = 0; 	// 0x0000
		authenticationData = 0; // 0x0000000000000000
	}
	
	public short getVersion() {
		return version;
	}
	
	public void setVersion(byte version) {
		this.version = version;
	}
	
	public short getType() {
		return type;
	}
	
	public void setType(byte type) {
		this.type = type;
	}
	
	public short getLength() {
		return length;
	}
	
	public void setLength(short length) {
		this.length = length;
	}
	
	public int getRouterID() {
		return routerID;
	}
	
	public void setRouterID(int routerID) {
		this.routerID = routerID;
	}
	
	public int getAreaID() {
		return areaID;
	}
	
	public void setAreaID(int areaID) {
		this.areaID = areaID;
	}
	
	public short getChecksum() {
		return checksum;
	}
	
	public void setChecksum(short checksum) {
		this.checksum = checksum;
	}
	
	public short getAuthentication() {
		return authentication;
	}
	
	public void setAuthentication(short authentication) {
		this.authentication = authentication;
	}
	
	public long getAuthenticationData() {
		return authenticationData;
	}
	
	public void setAuthenticationData(long authenticationData) {
		this.authenticationData = authenticationData;
	}
	
	public byte[] toByteArray() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		
		try {
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(version).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(type).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(length).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(routerID).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(areaID).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(checksum).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(authentication).array());
			outputStream.write(ByteBuffer.allocate(Long.BYTES).putLong(authenticationData).array());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return outputStream.toByteArray();
	}
	
	public byte[] toChecksumCalculationByteArray() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		
		try {
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(version).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(type).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(length).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(routerID).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(areaID).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort((short)0).array());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return outputStream.toByteArray();
	}
}