package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa.OSPFLSAHeader;
import org.opendaylight.tutorial.tutorial_L2_forwarding.util.DataPacketHelper;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*
RFC 2328                     OSPF Version 2                   April 1998
A.3.6 The Link State Acknowledgment packet

    Link State Acknowledgment Packets are OSPF packet type 5.  To make
    the flooding of LSAs reliable, flooded LSAs are explicitly
    acknowledged.  This acknowledgment is accomplished through the
    sending and receiving of Link State Acknowledgment packets.
    Multiple LSAs can be acknowledged in a single Link State
    Acknowledgment packet.

    Depending on the state of the sending interface and the sender of
    the corresponding Link State Update packet, a Link State
    Acknowledgment packet is sent either to the multicast address
    AllSPFRouters, to the multicast address AllDRouters, or as a
    unicast.  The sending of Link State Acknowledgement packets is
    documented in Section 13.5.  The reception of Link State
    Acknowledgement packets is documented in Section 13.7.

    The format of this packet is similar to that of the Data Description
    packet.  The body of both packets is simply a list of LSA headers.

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |   Version #   |       5       |         Packet length         |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          Router ID                            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                           Area ID                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |           Checksum            |             AuType            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                                                               |
       +-                                                             -+
       |                                                               |
       +-                         An LSA Header                       -+
       |                                                               |
       +-                                                             -+
       |                                                               |
       +-                                                             -+
       |                                                               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |
 */
public class OSPFLinkStateAcknowledgment extends OSPF {
	private List<OSPFLSAHeader> lsaHeaders;
	
	public OSPFLinkStateAcknowledgment(int routerID, int areaID, List<OSPFLSAHeader> lsaHeaders) {
		header = new OSPFHeader((byte)0x05);
		header.setRouterID(routerID);
		header.setAreaID(areaID);
		
		this.lsaHeaders = lsaHeaders;
	}
	
	public void setOSPFLSAHeader(List<OSPFLSAHeader> lsaHeaders) {
		this.lsaHeaders = lsaHeaders;
	}
	
	public void addOSPFLSAHeader(OSPFLSAHeader lsaHeader) {
		this.lsaHeaders.add(lsaHeader);
	}
	
	@Override
	public byte[] toByteArray() {
		calculateAndSetHeaderLengthAndChecksum();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(this.header.toByteArray());
			outputStream.write(getDefaultBodyBytes());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * calculate and set header length and checksum fields
	 */
	private void calculateAndSetHeaderLengthAndChecksum() {
		// calculate and set header length field
		short length = (short)(OSPFHeader.LENGTH + OSPFLSAHeader.LENGTH * this.lsaHeaders.size());
		this.header.setLength(length);
		
		// calculate and set header checksum field
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(this.header.toChecksumCalculationByteArray());
			outputStream.write(getDefaultBodyBytes());
			
			byte[] checksumBytes = DataPacketHelper.calculateOSPFChecksum(outputStream.toByteArray());
			short[] checksum = new short[checksumBytes.length/2];
			ByteBuffer.wrap(checksumBytes).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(checksum);
			this.header.setChecksum(checksum[checksum.length-1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private byte[] getDefaultBodyBytes() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		for(OSPFLSAHeader lsaHeader : this.lsaHeaders)
			try {
				outputStream.write(lsaHeader.toByteArray());
			} catch (IOException e) {
				e.printStackTrace();
			}
		return outputStream.toByteArray();
	}
}