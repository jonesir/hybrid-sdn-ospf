package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

public class RawDumper {
	public static void logRawDataPacket(byte[] content) {
    	StringBuilder sb = new StringBuilder();
    	for (int i = 1 ; i<content.length +1; i++) {
			sb.append(DataPacketHelper.byte2hex(content[i-1])+ " ");
			if (i%16==0){
				sb.append("\n");
			}
		}
		System.out.println(sb.toString());
    }
}
