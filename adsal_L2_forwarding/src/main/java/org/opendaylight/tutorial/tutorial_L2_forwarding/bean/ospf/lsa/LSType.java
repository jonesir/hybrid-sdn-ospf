package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

public class LSType {
	public static final short ROUTER_LSAs = 1;
	public static final short NETWORK_LSAs = 2;
	public static final short SUMMARY_LSAs_IP_NETWORK = 3;
	public static final short SUMMARY_LSAs_ASBR = 4;
	public static final short AS_EXTERNAL_LSAs = 5;
}
