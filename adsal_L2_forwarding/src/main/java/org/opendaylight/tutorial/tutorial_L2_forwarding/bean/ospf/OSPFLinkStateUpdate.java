package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa.OSPFLSA;
import org.opendaylight.tutorial.tutorial_L2_forwarding.util.DataPacketHelper;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*
RFC 2328
A.3.5 The Link State Update packet

    Link State Update packets are OSPF packet type 4.  These packets
    implement the flooding of LSAs.  Each Link State Update packet
    carries a collection of LSAs one hop further from their origin.
    Several LSAs may be included in a single packet.

    Link State Update packets are multicast on those physical networks
    that support multicast/broadcast.  In order to make the flooding
    procedure reliable, flooded LSAs are acknowledged in Link State
    Acknowledgment packets.  If retransmission of certain LSAs is
    necessary, the retransmitted LSAs are always sent directly to the
    neighbor.  For more information on the reliable flooding of LSAs,
    consult Section 13.

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |   Version #   |       4       |         Packet length         |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          Router ID                            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                           Area ID                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |           Checksum            |             AuType            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                            # LSAs                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                                                               |
       +-                                                            +-+
       |                             LSAs                              |
       +-                                                            +-+
       |                              ...                              |

 */
public class OSPFLinkStateUpdate extends OSPF {

	private static final short LSA_NUMBER_LENGTH = 4;
	private List<OSPFLSA> lsas;
	
	public OSPFLinkStateUpdate(int routerID, int areaID, List<OSPFLSA> lsas) {
		header = new OSPFHeader((byte)0x04);
		header.setRouterID(routerID);
		header.setAreaID(areaID);
		
		this.lsas = lsas;
	}
	
	public void addLinkStateAdvertisement(OSPFLSA lsa) {
		this.lsas.add(lsa);
	}
	
	@Override
	public byte[] toByteArray() {
		preparation();
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			// header bytes
			outputStream.write(this.header.toByteArray());
			outputStream.write(getDefaultBodyBytes());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * calculate and set header length and checksum fields
	 */
	private void preparation() {
		// calculate and set header length field
		short length = (short)(OSPFHeader.LENGTH + LSA_NUMBER_LENGTH);
		for (OSPFLSA lsa : this.lsas){
			length += lsa.getLength();
		}
		this.header.setLength(length);
		
		// calculate and set header checksum field
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			// header bytes
			outputStream.write(this.header.toChecksumCalculationByteArray());
			outputStream.write(getDefaultBodyBytes());
			
			byte[] checksumBytes = DataPacketHelper.calculateOSPFChecksum(outputStream.toByteArray());
			short[] checksum = new short[checksumBytes.length/2];
			ByteBuffer.wrap(checksumBytes).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(checksum);
			this.header.setChecksum(checksum[checksum.length-1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private byte[] getDefaultBodyBytes() {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(this.lsas.size()).array());
			for (OSPFLSA lsa : lsas)
				outputStream.write(lsa.toByteArray());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
