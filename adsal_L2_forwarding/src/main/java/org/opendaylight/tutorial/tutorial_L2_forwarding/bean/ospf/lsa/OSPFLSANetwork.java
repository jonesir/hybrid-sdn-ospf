package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*    
RFC 2328
A.4.3 Network-LSAs

    Network-LSAs are the Type 2 LSAs.  A network-LSA is originated for
    each broadcast and NBMA network in the area which supports two or
    more routers.  The network-LSA is originated by the network's
    Designated Router.  The LSA describes all routers attached to the
    network, including the Designated Router itself.  The LSA's Link
    State ID field lists the IP interface address of the Designated
    Router.

    The distance from the network to all attached routers is zero.  This
    is why metric fields need not be specified in the network-LSA.  For
    details concerning the construction of network-LSAs, see Section
    12.4.2.


        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |            LS age             |      Options  |      2        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                        Link State ID                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     Advertising Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     LS sequence number                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         LS checksum           |             length            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                         Network Mask                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                        Attached Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |
*/
public class OSPFLSANetwork extends OSPFLSA {
	private static final short DEFAULT_LENGTH = 8;
	private int networkMask;
	private int attachedRouter;
	
	public OSPFLSANetwork() {
		lsaHeader = new OSPFLSAHeader((byte)0x02);
		networkMask = 0;
		attachedRouter = 0;
	}
	
	public int getNetworkMask() {
		return networkMask;
	}

	public void setNetworkMask(int networkMask) {
		this.networkMask = networkMask;
	}

	public int getAttachedRouter() {
		return attachedRouter;
	}

	public void setAttachedRouter(int attachedRouter) {
		this.attachedRouter = attachedRouter;
	}

	@Override
	public byte[] toByteArray() {
		setHeaderChecksumAndLength();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(this.lsaHeader.toByteArray());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(networkMask).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(attachedRouter).array());
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setHeaderChecksumAndLength() {
		
	}
	
	@Override
	public short getLength() {
		short length = DEFAULT_LENGTH;
		return length;
	}
}
