package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa.OSPFLSATOS;

/**
 * 
 * @author Yuesheng Zhong
 *
 */
public class OSPFRouterLink {
	private static final short DEFAULT_LENGTH = 12;
	
	private int linkID;
	private int linkData;
	private byte type;
	private byte _tos;
	private short metric;
	private List<OSPFLSATOS> toss; 
	
	public OSPFRouterLink() {
		this.linkID = 0;
		this.linkData = 0;
		this.type = (byte)0x00;
		this._tos = 0;
		this.metric = 0;
		this.toss = new ArrayList<OSPFLSATOS>();
	}

	public int getLinkID() {
		return linkID;
	}

	public void setLinkID(int linkID) {
		this.linkID = linkID;
	}

	public int getLinkData() {
		return linkData;
	}

	public void setLinkData(int linkData) {
		this.linkData = linkData;
	}

	public byte getType() {
		return type;
	}

	public void setType(byte type) {
		this.type = type;
	}

	public byte get_tos() {
		return _tos;
	}

	public void set_tos(byte _tos) {
		this._tos = _tos;
	}

	public short getMetric() {
		return metric;
	}

	public void setMetric(short metric) {
		this.metric = metric;
	}

	public List<OSPFLSATOS> getToss() {
		return toss;
	}

	public void setToss(List<OSPFLSATOS> toss) {
		this.toss = toss;
	}
	
	public void addTOS(OSPFLSATOS tos) {
		this.toss.add(tos);
	}
	
	public short getLength() {
		return (short)(DEFAULT_LENGTH + toss.size() * OSPFLSATOS.LENGTH);
	}
	
	public byte[] toBytes() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(this.linkID).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(this.linkData).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(type).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(this._tos).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(this.metric).array());
			for(OSPFLSATOS tos : toss)
				outputStream.write(tos.toBytes());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
