package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*
RFC 2328 
A.4.1 The LSA header

    All LSAs begin with a common 20 byte header.  This header contains
    enough information to uniquely identify the LSA (LS type, Link State
    ID, and Advertising Router).  Multiple instances of the LSA may
    exist in the routing domain at the same time.  It is then necessary
    to determine which instance is more recent.  This is accomplished by
    examining the LS age, LS sequence number and LS checksum fields that
    are also contained in the LSA header.


        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |            LS age             |    Options    |    LS type    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                        Link State ID                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     Advertising Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     LS sequence number                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         LS checksum           |             length            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */
public class OSPFLSAHeader {
	public static final short LENGTH = 20;

	private short lsAge;
	private byte options;
	private byte lsType;
	private int linkStateID;
	private int advertisingRouter;
	private int lsSequenceNumber;
	private short lsChecksum;
	private short length;
	
	public OSPFLSAHeader(byte lsType) {
		lsAge = 0;
		options = 0;
		this.lsType = lsType;
		linkStateID = 0;
		advertisingRouter = 0;
		lsSequenceNumber = 0;
		lsChecksum = 0;
		length = 0;
	}
	
	public short getLsAge() {
		return lsAge;
	}

	public void setLsAge(short lsAge) {
		this.lsAge = lsAge;
	}

	public byte getOptions() {
		return options;
	}

	public void setOptions(byte options) {
		this.options = options;
	}

	public byte getLsType() {
		return lsType;
	}

	public void setLsType(byte lsType) {
		this.lsType = lsType;
	}

	public int getLinkStateID() {
		return linkStateID;
	}

	public void setLinkStateID(int linkStateID) {
		this.linkStateID = linkStateID;
	}

	public int getAdvertisingRouter() {
		return advertisingRouter;
	}

	public void setAdvertisingRouter(int advertisingRouter) {
		this.advertisingRouter = advertisingRouter;
	}

	public int getLsSequenceNumber() {
		return lsSequenceNumber;
	}

	public void setLsSequenceNumber(int lsSequenceNumber) {
		this.lsSequenceNumber = lsSequenceNumber;
	}

	public short getLsChecksum() {
		return lsChecksum;
	}

	public void setLsChecksum(short lsChecksum) {
		this.lsChecksum = lsChecksum;
	}

	public short getLength() {
		return length;
	}

	public void setLength(short length) {
		this.length = length;
	}

	public byte[] toByteArray() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		
		try {
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(lsAge).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(options).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(lsType).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(linkStateID).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(advertisingRouter).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(lsSequenceNumber).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(lsChecksum).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(length).array());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return outputStream.toByteArray();
	}
}