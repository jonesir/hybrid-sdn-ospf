package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFHeader;
import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFHello;

public class DataPacketAssembler {
	public static short IP_IDENTIFICATION = 100;
	public static final byte[] SOURCE_MAC_ADDRESS_BYTES = new byte[]{(byte)0x00 , (byte)0x1d , (byte)0x46 , (byte)0xdd , (byte)0x4c , (byte)0x90};
	public static final byte[] SOURCE_IP_ADDRESS_BYTES = new byte[]{(byte)0x0a, (byte)0x00, (byte)0x00, (byte)0x03};
	public static final byte[] BROADCAST_IP_ADDRESS_BYTES = new byte[]{(byte)0xe0, (byte)0x00, (byte)0x00, (byte)0x05};
	public static final byte[] ROUTER_ID_BYTES = new byte[]{(byte)0x0a, (byte)0x0b, (byte)0x0c, (byte)0x0d};
	public static final String ROUTER_ID_STRING = "0a0b0c0d";
	public static final byte[] AREA_ID_BYTES = new byte[]{(byte)0x00, (byte)0x00, (byte)0x00, (byte)0x00};
	public static final byte[] OSPF_LLSDB_BYTES = new byte[]{(byte)0xff, (byte)0xf6, (byte)0x00, (byte)0x03, (byte)0x00, (byte)0x01, (byte)0x00, (byte)0x04, (byte)0x00, (byte)0x00, (byte)0x00, (byte)0x01};
    
	public static final int ETHERNET_HEADER_LENGTH = 14;
	public static final int ETHERNET_DESTINATION_OFFSET = 0;
	public static final int ETHERNET_SOURCE_OFFSET = 6;
    
	public static final int IP_HEADER_LENGTH = 20;
	public static final int IP_SOURCE_OFFSET = 26;
	public static final int IP_DESTINATION_OFFSET = 30;
    
	/** General OSPF Packet Header Fields */
	public static final int OSPF_DATA_PACKET_VERSION_OFFSET = 34;
	public static final int OSPF_DATA_PACKET_TYPE_OFFSET = 35;
	public static final int OSPF_DATA_PACKET_LENGTH_OFFSET = 36;
	public static final int OSPF_DATA_PACKET_ROUTER_ID_OFFSET = 38;
	public static final int OSPF_DATA_PACKET_AREA_ID_OFFSET = 42;
	public static final int OSPF_DATA_PACKET_CHECKSUM_OFFSET = 46;
	public static final int OSPF_DATA_PACKET_AUTHENTICATION_OFFSET = 48;
	public static final int OSPF_DATA_PACKET_AUTHENTICATION_DATA_OFFSET = 50;
    
    /** HELLO */
	public static final int OSPF_HELLO_DATA_PACKET_NETWORK_MASK_OFFSET = 58;
	public static final int OSPF_HELLO_DATA_PACKET_HELLO_INTERVAL_OFFSET = 62;
	public static final int OSPF_HELLO_DATA_PACKET_OPTIONS_OFFSET = 64;
	public static final int OSPF_HELLO_DATA_PACKET_PRIORITY_OFFSET = 65;
	public static final int OSPF_HELLO_DATA_PACKET_HELLO_DEATH_INTERVAL_OFFSET = 66;
	public static final int OSPF_HELLO_DATA_PACKET_DESIGNATED_ROUTER_OFFSET = 70;
	public static final int OSPF_HELLO_DATA_PACKET_BACKUP_DESIGNATED_ROUTER_OFFSET = 74;
	public static final int OSPF_HELLO_DATA_PACKET_NEIGHBOR_OFFSET = 78;
	public static final int OSPF_HELLO_DATA_PACKET_SOURCE_ROUTER_ID_OFFSET = 38;
	public static final int OSPF_HELLO_DATA_PACKET_LENGTH_WITHOUT_NEIGHBOR = 90;
    
	public static final int OSPF_DATA_PACKET_AUTHENTICATION_LENGTH = 2;
	public static final int OSPF_DATA_PACKET_AUTHENTICATION_DATA_LENGTH = 8;
    
    public static byte[] assembleOSPFHelloPacket(byte[] dataPacket, Map<String, byte[]> neighbors) {
    	List<Byte> ospfDataPacketList = new ArrayList<Byte>();
    	
    	prepareCommonPart(ospfDataPacketList, dataPacket);
    	
    	setOSPFHelloFields(ospfDataPacketList, dataPacket, neighbors);
    	
		return DataPacketHelper.list2bytes(ospfDataPacketList);
    }
    
    public static byte[] assembleOSPFDatabaseDescriptionPacket(byte[] dataPacket) {
    	List<Byte> ospfDataPacketList = new ArrayList<Byte>();
    	prepareCommonPart(ospfDataPacketList, dataPacket);
    	
    	
    	return null;
    }
    
    public static byte[] assembleOSPFLinkStateRequestPacket(byte[] dataPacket) {
    	
    	return null;
    }
    
    public static byte[] assembleOSPFLinkStateUpdatePacket(byte[] dataPacket) {
    	
    	return null;
    }
    
    public static byte[] assembleOSPFLinkStateAcknowledgementPacket(byte[] dataPacket) {
    	
    	return null;
    }
    
    public static OSPFHello decodeOSPFHello(byte[] dataPacket) {
    	OSPFHello hello = new OSPFHello(0,0,0,0,null);
    	// decode header
    	OSPFHeader header = hello.getHeader(); 
    	header.setVersion(dataPacket[OSPF_DATA_PACKET_VERSION_OFFSET]);
    	header.setLength(ByteBuffer.wrap(dataPacket, OSPF_DATA_PACKET_LENGTH_OFFSET, 2).getShort());
    	header.setRouterID(ByteBuffer.wrap(dataPacket, OSPF_DATA_PACKET_ROUTER_ID_OFFSET, 4).getInt());
    	header.setAreaID(ByteBuffer.wrap(dataPacket, OSPF_DATA_PACKET_AREA_ID_OFFSET, 4).getInt());
    	header.setChecksum(ByteBuffer.wrap(dataPacket, OSPF_DATA_PACKET_CHECKSUM_OFFSET, 2).getShort());
    	header.setAuthentication(ByteBuffer.wrap(dataPacket, OSPF_DATA_PACKET_AUTHENTICATION_OFFSET, 2).getShort());
    	header.setAuthenticationData(ByteBuffer.wrap(dataPacket, OSPF_DATA_PACKET_AUTHENTICATION_DATA_OFFSET, 8).getLong());
    	
    	// decode hello body
    	hello.setNetworkmask(ByteBuffer.wrap(dataPacket, OSPF_HELLO_DATA_PACKET_NETWORK_MASK_OFFSET, 4).getInt());
    	hello.setHelloInterval(ByteBuffer.wrap(dataPacket, OSPF_HELLO_DATA_PACKET_HELLO_INTERVAL_OFFSET, 2).getShort());
    	hello.setOptions(dataPacket[OSPF_HELLO_DATA_PACKET_OPTIONS_OFFSET]);
    	hello.setPriority(dataPacket[OSPF_HELLO_DATA_PACKET_PRIORITY_OFFSET]);
    	hello.setRouterDeadInterval(ByteBuffer.wrap(dataPacket, OSPF_HELLO_DATA_PACKET_HELLO_DEATH_INTERVAL_OFFSET, 4).getInt());
    	hello.setDesignatedRouter(ByteBuffer.wrap(dataPacket, OSPF_HELLO_DATA_PACKET_DESIGNATED_ROUTER_OFFSET, 4).getInt());
    	hello.setBackupDesignatedRouter(ByteBuffer.wrap(dataPacket, OSPF_HELLO_DATA_PACKET_BACKUP_DESIGNATED_ROUTER_OFFSET, 4).getInt());
    	
    	// decode neighbors
    	int nb = (dataPacket.length - OSPF_HELLO_DATA_PACKET_LENGTH_WITHOUT_NEIGHBOR) /4;
    	for (int i = 0 ; i < nb ; i++)
    		hello.addNeighbor(ByteBuffer.wrap(dataPacket, OSPF_HELLO_DATA_PACKET_NEIGHBOR_OFFSET + 4*i, 4).getInt());
    	
    	return hello;
    }
    
    private static void prepareCommonPart(List<Byte> ospfDataPacketList, byte[] dataPacket) {
    	setEthernetFields(ospfDataPacketList, dataPacket);
    	setIPFields(ospfDataPacketList, dataPacket);
    }
    
    private static void setEthernetFields(List<Byte> ospfDataPacketList, byte[] dataPacket) {
    	/** set destination MAC address*/
    	for(int i = 0 ; i < 6 ; i++)
    		ospfDataPacketList.add((byte)0x00);
    	
    	/** set source MAC address */
    	for(int i = 0 ; i < 6 ; i++)
    		ospfDataPacketList.add((byte)0x00);
    	
    	/** set protocol : (08 00) */
		ospfDataPacketList.add((byte)0x08);
		ospfDataPacketList.add((byte)0x00);
    }
	
	private static void setIPFields(List<Byte> ospfDataPacketList, byte[] dataPacket) {
		/** set IP version and header length */
		ospfDataPacketList.add((byte)0x45);
		
		/** set Differentiated Service CodePoint 0xc0*/
    	ospfDataPacketList.add((byte)0xc0);
    	
    	/** prepare length in IP Header */
    	for (int i = 0 ; i < 2 ; i++ ) 
    		ospfDataPacketList.add((byte)0x00);
    	
    	/** set identification in IP header */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		
		/** set fragment and offset */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		
		/** set TTL : 01 */
		ospfDataPacketList.add((byte)0x01);
		
		/** set protocol number : 59 */
		ospfDataPacketList.add((byte)0x59);
		
		/** prepare checksum */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		
		/** prepare source IP Address */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		
		/** prepare destination IP Address */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
    }
	
	private static void setOSPFHeader(List<Byte> dataByteList, byte type) {
    	/** set OSPF version : 02 */
    	dataByteList.add((byte)0x02);
    	
    	/** set OSPF type */
    	dataByteList.add(type);
    	
    	/** place holder for packet length which takes 2 bytes */
    	dataByteList.add((byte)0x00);
		dataByteList.add((byte)0x00);
    	
    	/** set Router ID */
    	for (byte b : ROUTER_ID_BYTES)
    		dataByteList.add(b);
    	
    	/** set Area ID */
    	for (byte b : AREA_ID_BYTES)
    		dataByteList.add(b);
    	
    	/** place holder for checksum which takes 2 bytes */
    	dataByteList.add((byte)0x00);
		dataByteList.add((byte)0x00);
    	
    	/** set authentication type which takes 2 bytes */
		dataByteList.add((byte)0x00);
		dataByteList.add((byte)0x00);
    	
    	/** set authentication data which takes 8 bytes */
    	for (int i = 0 ; i < 8 ; i++)
    		dataByteList.add((byte)0x00);
    }
	
	private static void setOSPFHelloFields(List<Byte> ospfDataPacketList, byte[] dataPacket, Map<String, byte[]> neighbors) {
    	DataPacketAssembler.setOSPFHeader(ospfDataPacketList, (byte)0x01);
		
		//: UNCHANGED (ff ff ff 00)
		/** set network mask */
		ospfDataPacketList.add((byte)0xff);
		ospfDataPacketList.add((byte)0xff);
		ospfDataPacketList.add((byte)0xff);
		ospfDataPacketList.add((byte)0x00);
		
		//: UNCHANGED (00 0a)
		/** set hello interval */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x0a);
		
		//: UNCHANGED (12)
		/** set options */
		ospfDataPacketList.add((byte)0x12);
		
		//: UNCHANGED (01) 
		/** set priority */
		ospfDataPacketList.add((byte)0x01);
		
		// : UNCHANGED (00 00 00 28)
		/** set hello death interval */
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x00);
		ospfDataPacketList.add((byte)0x28);
		
		/** set Designated Router IP */
		for(byte b : SOURCE_IP_ADDRESS_BYTES)
			ospfDataPacketList.add(b);
		
		/** set Backup Designated Router IP */
		ospfDataPacketList.add(dataPacket[74]);
		ospfDataPacketList.add(dataPacket[75]);
		ospfDataPacketList.add(dataPacket[76]);
		ospfDataPacketList.add(dataPacket[77]);
		
		/** set first neighbor(s) ID */
		appendNeighbors(ospfDataPacketList, dataPacket, neighbors);
		
		// : UNCHANGED (ff f6 00 03 00 01 00 04 00 00 00 01)
		/** set OSPF LLSDB */
		setLLSDBBytes(ospfDataPacketList);
		
		//-------------------------------------MODIFY----------------------------------------
		
		/** set OSPF total length */
		long totalLength = ospfDataPacketList.size()
				- ETHERNET_HEADER_LENGTH
				- IP_HEADER_LENGTH
				- OSPF_LLSDB_BYTES.length
				;
		
		byte[] length = ByteBuffer.allocate(Long.BYTES).putLong(totalLength).array();
		ospfDataPacketList.set(OSPF_DATA_PACKET_LENGTH_OFFSET, length[6]);
		ospfDataPacketList.set(OSPF_DATA_PACKET_LENGTH_OFFSET + 1, length[7]);
		
		// create temp byte array for checksum calculation
		int lengthOfDataPacketForChecksum = ((int)totalLength) - OSPF_DATA_PACKET_AUTHENTICATION_LENGTH - OSPF_DATA_PACKET_AUTHENTICATION_DATA_LENGTH;
		if(lengthOfDataPacketForChecksum%2!=0)
			lengthOfDataPacketForChecksum++;
		
		byte[] dataPacketForChecksum = new byte[lengthOfDataPacketForChecksum];
		dataPacketForChecksum[lengthOfDataPacketForChecksum -1] = (byte)0x00;
		
		int counter = 0 ;
		for (int j = OSPF_DATA_PACKET_VERSION_OFFSET ; j < OSPF_DATA_PACKET_AUTHENTICATION_OFFSET ; j++)
			dataPacketForChecksum[counter++] = ospfDataPacketList.get(j);
		
		for (int j = OSPF_HELLO_DATA_PACKET_NETWORK_MASK_OFFSET ; j < ospfDataPacketList.size() - OSPF_LLSDB_BYTES.length ; j++) 
			dataPacketForChecksum[counter++] = ospfDataPacketList.get(j);
		
		/** Set OSPF Checksum */ 
		long checksumL = InternetChecksum.checksum(dataPacketForChecksum, lengthOfDataPacketForChecksum);
		byte[] checksum = ByteBuffer.allocate(Long.BYTES).putLong(checksumL).array();
		ospfDataPacketList.set(OSPF_DATA_PACKET_CHECKSUM_OFFSET, checksum[6]);
		ospfDataPacketList.set(OSPF_DATA_PACKET_CHECKSUM_OFFSET + 1, checksum[7]);
    }
	
	private static void appendNeighbors(List<Byte> dataByteList, byte[] dataPacket, Map<String, byte[]> neighbors) {
    	saveNeighbors(dataPacket, neighbors);
    	for (String key : neighbors.keySet())
    		for (byte b : neighbors.get(key))
    			dataByteList.add(b);
    }
    
    private static void setLLSDBBytes(List<Byte> dataByteList) {
    	for (byte b : OSPF_LLSDB_BYTES)
    		dataByteList.add(b);
    }
    
    private static void saveNeighbors(byte[] dataPacket, Map<String, byte[]> neighbors) {
    	int numberOfNumbers = (dataPacket.length - OSPF_HELLO_DATA_PACKET_LENGTH_WITHOUT_NEIGHBOR) / 4;
    	StringBuilder key = new StringBuilder();
    	byte[] possibleNeighbor;
    	for (int i = 0 ; i < numberOfNumbers ; i++) {
    		possibleNeighbor = DataPacketHelper.createByteArray(dataPacket, OSPF_HELLO_DATA_PACKET_NEIGHBOR_OFFSET + i*4, 4);
    		
    		key.delete(0, key.length());
    		for (byte b : possibleNeighbor)
    			key.append(DataPacketHelper.byte2hex(b));
    		if (!neighbors.containsKey(key.toString()) && (!ROUTER_ID_STRING.equals(key.toString())))
    			neighbors.put(key.toString(), possibleNeighbor);
    	}
    	
    	//add current incoming data packet's source Router ID into neighbor list
    	possibleNeighbor = new byte[]{
    			dataPacket[OSPF_HELLO_DATA_PACKET_SOURCE_ROUTER_ID_OFFSET],
    			dataPacket[OSPF_HELLO_DATA_PACKET_SOURCE_ROUTER_ID_OFFSET + 1],
    			dataPacket[OSPF_HELLO_DATA_PACKET_SOURCE_ROUTER_ID_OFFSET + 2],
    			dataPacket[OSPF_HELLO_DATA_PACKET_SOURCE_ROUTER_ID_OFFSET + 3]
    			};
    	key.delete(0, key.length());
    	for (byte b : possibleNeighbor)
    		key.append(b);
    	
    	if(!neighbors.containsKey(key.toString()))
    		neighbors.put(key.toString(), possibleNeighbor);
    }
}