package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;



/**
 * 
 * @author Yuesheng Zhong
 *
 */
public abstract class OSPFLSA {
	protected OSPFLSAHeader lsaHeader;
	public abstract byte[] toByteArray();
	public abstract short getLength();
}