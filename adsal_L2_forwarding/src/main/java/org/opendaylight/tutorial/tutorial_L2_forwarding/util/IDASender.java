package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

public class IDASender {
	static DatagramSocket routerSocket = null;
	
	static {
		try {
			 routerSocket = new DatagramSocket(65432);
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}
	
	public static void send(byte[] content,String ipAddress, int portRemote,  String intf, int portLocal) {
//		Socket s;
//		DataOutputStream out = null;
		try {
			
			DatagramPacket sendPacket = new DatagramPacket(content, content.length, InetAddress.getByName(ipAddress), portRemote);
//			s = new Socket(InetAddress.getByName(ipAddress),portRemote, InetAddress.getByName(intf), portLocal);
//			out = new DataOutputStream(new BufferedOutputStream(s.getOutputStream()));
//			out.write(content);
//			out.close();
			if (routerSocket!=null) {
				routerSocket.send(sendPacket);
				routerSocket.close();
			}
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		} finally {
//			if (out!=null){
//				try {
//					out.close();
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}

		}
		
	}
}
