package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

/**
 * 
 * @author Yuesheng Zhong
 *
 */
public class OSPFLogger {
	public static void logHello() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("D    D   DDDDDD   D      D       DDDD\n");
    	sb.append("D    D   D        D      D      D    D\n");
    	sb.append("DDDDDD   DDDDDD   D      D      D    D\n");
    	sb.append("D    D   D        D      D      D    D\n");
    	sb.append("D    D   DDDDDD   DDDDDD DDDDDD  DDDD\n");
    	
    	System.out.println(sb.toString());
    }
	
	public static void logDataDescription() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("DDDDD    DDDDD    DDDDD\n");
    	sb.append("D    D   D    D   D    D\n");
    	sb.append("D    D   DDDDD    D    D\n");
    	sb.append("D    D   D    D   D    D\n");
    	sb.append("DDDDD    DDDDD    DDDDD\n");
    	
    	System.out.println(sb.toString());
    }
	
	public static void logLinkStateRequest() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("D        DDDDDD   DDDDD\n");
    	sb.append("D        D        D    D\n");
    	sb.append("D        DDDDDD   DDDDD\n");
    	sb.append("D             D   D DDD\n");
    	sb.append("DDDDDDD  DDDDDD   D  DDDD\n");
    	
    	System.out.println(sb.toString());
    }
	
	public static void logLinkStateUpdate() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("D        DDDDDD   D    D\n");
    	sb.append("D        D        D    D\n");
    	sb.append("D        DDDDDD   D    D\n");
    	sb.append("D             D   D    D\n");
    	sb.append("DDDDDDD  DDDDDD    DDDD\n");
    	
    	System.out.println(sb.toString());
    }
	
	public static void logLinkStateAcknowledge() {
    	StringBuilder sb = new StringBuilder();
    	sb.append("D        DDDDDD    DDDD\n");
    	sb.append("D        D        D    D\n");
    	sb.append("D        DDDDDD   DDDDDD\n");
    	sb.append("D             D   D    D\n");
    	sb.append("DDDDDDD  DDDDDD   D    D\n");
    	
    	System.out.println(sb.toString());
    }
}
