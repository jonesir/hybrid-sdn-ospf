package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*    
RFC 2328                     OSPF Version 2                   April 1998

A.4.4 Summary-LSAs

    Summary-LSAs are the Type 3 and 4 LSAs.  These LSAs are originated
    by area border routers. Summary-LSAs describe inter-area
    destinations.  For details concerning the construction of summary-
    LSAs, see Section 12.4.3.

    Type 3 summary-LSAs are used when the destination is an IP network.
    In this case the LSA's Link State ID field is an IP network number
    (if necessary, the Link State ID can also have one or more of the
    network's "host" bits set; see Appendix E for details). When the
    destination is an AS boundary router, a Type 4 summary-LSA is used,
    and the Link State ID field is the AS boundary router's OSPF Router
    ID.  (To see why it is necessary to advertise the location of each
    ASBR, consult Section 16.4.)  Other than the difference in the Link
    State ID field, the format of Type 3 and 4 summary-LSAs is
    identical.


        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |            LS age             |     Options   |    3 or 4     |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                        Link State ID                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     Advertising Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     LS sequence number                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         LS checksum           |             length            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                         Network Mask                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |      0        |                  metric                       |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |     TOS       |                TOS  metric                    |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |
*/
public class OSPFLSASummary extends OSPFLSA {
	private static final short DEFAULT_LENGTH = 12;
	private byte extra;
	private int metric;
	private byte tos;
	private int tosMetric;
	
	public OSPFLSASummary() {
		lsaHeader = new OSPFLSAHeader((byte)0x03);
		extra = 0;
		metric = 0;
		tos = 0;
		tosMetric = 0;
	}
	
	public byte getExtra() {
		return extra;
	}

	public void setExtra(byte extra) {
		this.extra = extra;
	}

	public int getMetric() {
		return metric;
	}

	public void setMetric(int metric) {
		this.metric = metric;
	}

	public byte getTos() {
		return tos;
	}

	public void setTos(byte tos) {
		this.tos = tos;
	}

	public int getTosMetric() {
		return tosMetric;
	}

	public void setTosMetric(int tosMetric) {
		this.tosMetric = tosMetric;
	}

	@Override
	public byte[] toByteArray() {
		setHeaderChecksumAndLength();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(this.lsaHeader.toByteArray());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(extra).array());
			byte[] metricArray = ByteBuffer.allocate(Integer.BYTES).putInt(metric).array();
			outputStream.write(new byte[]{metricArray[1],metricArray[2],metricArray[3]});
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(tos).array());
			byte[] tosMetricArray = ByteBuffer.allocate(Integer.BYTES).putInt(tosMetric).array();
			outputStream.write(new byte[]{tosMetricArray[1],tosMetricArray[2],tosMetricArray[3]});
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setHeaderChecksumAndLength() {
		
	}
	
	@Override
	public short getLength() {
		short length = DEFAULT_LENGTH;
		return length;
	}
}
