package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.util.DataPacketHelper;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*
RFC 2328


A.3.4 The Link State Request packet

    Link State Request packets are OSPF packet type 3.  After exchanging
    Database Description packets with a neighboring router, a router may
    find that parts of its link-state database are out-of-date.  The
    Link State Request packet is used to request the pieces of the
    neighbor's database that are more up-to-date.  Multiple Link State
    Request packets may need to be used.

    A router that sends a Link State Request packet has in mind the
    precise instance of the database pieces it is requesting. Each
    instance is defined by its LS sequence number, LS checksum, and LS
    age, although these fields are not specified in the Link State
    Request Packet itself.  The router may receive even more recent
    instances in response.

    The sending of Link State Request packets is documented in Section
    10.9.  The reception of Link State Request packets is documented in
    Section 10.7.

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |   Version #   |       3       |         Packet length         |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          Router ID                            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                           Area ID                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |           Checksum            |             AuType            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          LS type                              |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Link State ID                           |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     Advertising Router                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |

 */
public class OSPFLinkStateRequest extends OSPF {
	private List<LinkState> lss;
	
	public OSPFLinkStateRequest(int routerID, int areaID, List<LinkState> lss) {
		header = new OSPFHeader((byte)0x03);//0x03
		header.setRouterID(routerID);
		header.setAreaID(areaID);
		
		this.lss = lss;
	}
	
	public List<LinkState> getLinkStateList() {
		return this.lss;
	}

	public void addLinkState(LinkState ls) {
		if(this.lss==null)
			this.lss = new ArrayList<LinkState>();
		this.lss.add(ls);
	}
	
	@Override
	public byte[] toByteArray() {
		setHeaderLengthAndChecksum();
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			// write header first
			outputStream.write(this.header.toByteArray());
			outputStream.write(getDefaultBodyBytes());
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * calculate and set header length and checksum fields
	 */
	private void setHeaderLengthAndChecksum() {
		// calculate and set header length field
		short length = (short)(OSPFHeader.LENGTH + lss.size() * LinkState.DEFAULT_LINK_STATE_LENGTH);
		this.header.setLength(length);
		
		// calculate and set header checksum field
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(this.header.toChecksumCalculationByteArray());
			outputStream.write(getDefaultBodyBytes());
			
			byte[] checksumBytes = DataPacketHelper.calculateOSPFChecksum(outputStream.toByteArray());
			short[] checksum = new short[checksumBytes.length/2];
			ByteBuffer.wrap(checksumBytes).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(checksum);
			this.header.setChecksum(checksum[checksum.length-1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private byte[] getDefaultBodyBytes() {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			
			for (LinkState ls : lss) {
				outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(ls.lsType).array());
				outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(ls.linkStateID).array());
				outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(ls.advertisingRouter).array());
			}
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public class LinkState{
		public static final short DEFAULT_LINK_STATE_LENGTH = 12;
		private int lsType;
		private int linkStateID;
		private int advertisingRouter;
		
		public LinkState() {
			lsType = 1;				//0x00000001
			linkStateID = 0;		//0x00000000
			advertisingRouter = 0;	//0x00000000
		}
	}
}
