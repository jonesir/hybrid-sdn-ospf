package org.opendaylight.tutorial.tutorial_L2_forwarding.util;

import java.nio.ByteBuffer;
import java.util.List;

public class DataPacketHelper {
	public static String byte2hex(byte b) {
    	String result = Integer.toHexString((int)(b&0xff));
    	return result.length()==1 ? "0" + result : result; 
    }
	
	public static byte[] calculateOSPFChecksum(Byte[] data) {
    	byte[] checksumTMP = new byte[data.length];
    	for(int i = 0 ; i < data.length ; i++)
    		checksumTMP[i] = data[i];
    	long checksumResult = InternetChecksum.checksum(checksumTMP, checksumTMP.length);
    	return ByteBuffer.allocate(Long.BYTES).putLong(checksumResult).array();
    }
	
	public static byte[] calculateOSPFChecksum(byte[] ospfData) {
//    	
//    	byte[] checksumTMP;
		long checksumResult = 0;
//		int length = ((ospfData[36])<<8) + ((Byte)ospfData[37]).intValue();
//		
//		checksumTMP = new byte[length];
//		for(int i = 0 ; i < length ; i++){
//			checksumTMP[i] = ospfData[34+i];
//		}
//		
//		// set checksum field to 0 before computing checksum
//		checksumTMP[12] = 0x00;
//		checksumTMP[13] = 0x00;
//		
		// compute checksum
		checksumResult = InternetChecksum.checksum(ospfData, ospfData.length);
		//checksumResult = InternetChecksum.checksum(checksumTMP, checksumTMP.length);
		return ByteBuffer.allocate(Long.BYTES).putLong(checksumResult).array();
    }
	
	public static byte[] createByteArray(byte[] dataPacket, int offset, int length) {
    	byte[] result = new byte[length];
    	for (int i = 0 ; i < length ; i++)
    		result[i] = dataPacket[offset + i];
    	
    	return result;
    }
	
	public static byte[] list2bytes(List<Byte> dataPacketList) {
		byte[] finalPacket = new byte[dataPacketList.size()];
		for(int i = 0 ; i < dataPacketList.size() ; i++) {
			finalPacket[i] = dataPacketList.get(i);
		}
		
		return finalPacket;
	}
    
}
