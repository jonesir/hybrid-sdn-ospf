package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

/**
 * 
 * @author Yuesheng Zhong
 *
 */
public class RouterLinkType {
	public static final byte POINT_2_POINT_CONNECTION_TO_ANOTHER_ROUTER = 1;
	public static final byte CONNECTION_TO_A_TRANSIT_NETWORK = 2;
	public static final byte CONNECTION_TO_A_STUB_NETWORK = 3;
	public static final byte VIRTUAL_LINK = 4;
}
