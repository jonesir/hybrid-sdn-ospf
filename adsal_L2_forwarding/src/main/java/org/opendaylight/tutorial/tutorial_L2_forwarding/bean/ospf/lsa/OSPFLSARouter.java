package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.OSPFRouterLink;

/**
 * 
 * @author Yuesheng Zhong
 *
 */

/*    
 	 RFC 2328
	 A.4.2 Router-LSAs
	
	    Router-LSAs are the Type 1 LSAs.  Each router in an area originates
	    a router-LSA.  The LSA describes the state and cost of the router's
	    links (i.e., interfaces) to the area.  All of the router's links to
	    the area must be described in a single router-LSA.  For details
	    concerning the construction of router-LSAs, see Section 12.4.1. 
    
        0                   1                   2                   3
		0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|            LS age             |     Options   |       1       |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                        Link State ID                          |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                     Advertising Router                        |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                     LS sequence number                        |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|         LS checksum           |             length            |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|    0    |V|E|B|        0      |            # links            |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                          Link ID                              |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                         Link Data                             |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|     Type      |     # TOS     |            metric             |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                              ...                              |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|      TOS      |        0      |          TOS  metric          |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                          Link ID                              |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                         Link Data                             |
		+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
		|                              ...                              |
*/
public class OSPFLSARouter extends OSPFLSA {
	private static final short DEFAULT_LENGTH = 4;
	private short extra;
	private short _links;
	private List<OSPFRouterLink> routerLinks;
	
	public OSPFLSARouter() {
		this.lsaHeader = new OSPFLSAHeader((byte)0x01);
		this.extra = 0;
		this.routerLinks = new ArrayList<OSPFRouterLink>();
	}
	
	public short getExtra() {
		return extra;
	}

	public void setExtra(short extra) {
		this.extra = extra;
	}

	public short get_links() {
		return _links;
	}

	public void set_links(short _links) {
		this._links = _links;
	}

	public List<OSPFRouterLink> getRouterLinks() {
		return routerLinks;
	}

	public void setRouterLinks(List<OSPFRouterLink> routerLinks) {
		this.routerLinks = routerLinks;
	}

	public void addRouterLink(OSPFRouterLink routerLink) {
		this.routerLinks.add(routerLink);
	}
	
	@Override
	public byte[] toByteArray() {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			outputStream.write(this.lsaHeader.toByteArray());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(this.extra).array());
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(this._links).array());
			for(OSPFRouterLink routerLink : this.routerLinks)
				outputStream.write(routerLink.toBytes());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public short getLength() {
		short length = DEFAULT_LENGTH;
		for (OSPFRouterLink routerLink : routerLinks)
			length += routerLink.getLength();
		
		return length;
	}
}
