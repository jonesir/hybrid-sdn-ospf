package org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;

import org.opendaylight.tutorial.tutorial_L2_forwarding.bean.ospf.lsa.OSPFLSAHeader;
import org.opendaylight.tutorial.tutorial_L2_forwarding.util.DataPacketHelper;

/*
   RFC 2328
	A.3.3 The Database Description packet

    Database Description packets are OSPF packet type 2.  These packets
    are exchanged when an adjacency is being initialized.  They describe
    the contents of the link-state database.  Multiple packets may be
    used to describe the database.  For this purpose a poll-response
    procedure is used.  One of the routers is designated to be the
    master, the other the slave.  The master sends Database Description
    packets (polls) which are acknowledged by Database Description
    packets sent by the slave (responses).  The responses are linked to
    the polls via the packets' DD sequence numbers.

        0                   1                   2                   3
        0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |   Version #   |       2       |         Packet length         |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                          Router ID                            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                           Area ID                             |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |           Checksum            |             AuType            |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                       Authentication                          |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |         Interface MTU         |    Options    |0|0|0|0|0|I|M|MS
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                     DD sequence number                        |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                                                               |
       +-                                                             -+
       |                                                               |
       +-                      An LSA Header                          -+
       |                                                               |
       +-                                                             -+
       |                                                               |
       +-                                                             -+
       |                                                               |
       +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
       |                              ...                              |
 */

/**
 * 
 * @author Yuesheng Zhong
 *
 */
public class OSPFDatabaseDescription extends OSPF {
	public static final short DEFAULT_DATABASE_DESCRIPTION_BODY_LENGTH = 8;
	public static int DDSequenceNumber = 100;
	private short interfaceMTU;
	private byte options;
	private byte extra;
	private int ddSequenceNumber;
	private List<OSPFLSAHeader> lsaHeaders;
	
	/**
	 * only extra and lsa headers are not default
	 */
	public OSPFDatabaseDescription(int routerID, int areaID, int init, int more, int master, List<OSPFLSAHeader> lsaHeaders){
		header = new OSPFHeader((byte)0x02);
		header.setRouterID(routerID);
		header.setAreaID(areaID);
		
		interfaceMTU = (short)1500;//0x05dc
		options = 82;//0x52
		extra = (byte)(((init<<2)+(more<<1)+master) & 0x07);// set the I|M|MS bits
		ddSequenceNumber = DDSequenceNumber++;
		this.lsaHeaders = lsaHeaders;
	}
	
	public short getInterfaceMTU() {
		return interfaceMTU;
	}

	public void setInterfaceMTU(short interfaceMTU) {
		this.interfaceMTU = interfaceMTU;
	}

	public byte getOptions() {
		return options;
	}

	public void setOptions(byte options) {
		this.options = options;
	}

	public byte getExtra() {
		return extra;
	}

	public void setExtra(byte extra) {
		this.extra = extra;
	}

	public int getDdSequenceNumber() {
		return ddSequenceNumber;
	}

	public void setDdSequenceNumber(int ddSequenceNumber) {
		this.ddSequenceNumber = ddSequenceNumber;
	}

	public List<OSPFLSAHeader> getLsaHeaders() {
		return lsaHeaders;
	}

	public void setLsaHeaders(List<OSPFLSAHeader> lsaHeaders) {
		this.lsaHeaders = lsaHeaders;
	}

	public void addLsaHeader(OSPFLSAHeader lsaHeader) {
		this.lsaHeaders.add(lsaHeader);
	}
	
	@Override
	public byte[] toByteArray() {
		calculateAndSetHeaderLengthAndChecksum();
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			// header bytes
			outputStream.write(this.header.toByteArray());
			// body bytes
			outputStream.write(getDefaultBodyBytes());
			// LLSDB bytes
			outputStream.write(this.llsdb.toByteArray());
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * calculate and set the header length and checksum fields
	 */
	private void calculateAndSetHeaderLengthAndChecksum() {
		// calculate and set header length field
		short length = (short)(OSPFHeader.LENGTH + OSPFDatabaseDescription.DEFAULT_DATABASE_DESCRIPTION_BODY_LENGTH + this.lsaHeaders.size() * OSPFLSAHeader.LENGTH); 
		this.header.setLength(length);
		
		// calculate and set header checksum field
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(this.header.toChecksumCalculationByteArray());// without authentication fields
			outputStream.write(getDefaultBodyBytes());
			
			byte[] checksumBytes = DataPacketHelper.calculateOSPFChecksum(outputStream.toByteArray());
			short[] checksum = new short[checksumBytes.length/2];
			ByteBuffer.wrap(checksumBytes).order(ByteOrder.BIG_ENDIAN).asShortBuffer().get(checksum);
			this.header.setChecksum(checksum[checksum.length-1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private byte[] getDefaultBodyBytes() {
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(ByteBuffer.allocate(Short.BYTES).putShort(interfaceMTU).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(options).array());
			outputStream.write(ByteBuffer.allocate(Byte.BYTES).put(extra).array());
			outputStream.write(ByteBuffer.allocate(Integer.BYTES).putInt(ddSequenceNumber).array());
			for (OSPFLSAHeader lsaHeader : lsaHeaders)
				outputStream.write(lsaHeader.toByteArray());
			
			return outputStream.toByteArray();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	public static void main(String[] args) {
		List<Integer> in = new ArrayList<Integer>();
		in.add(52321);
		in.add(52221);
		in.add(54441);
		in.add(56661);
		in.add(56621);
		in.add(12321);
		System.out.println(in.contains(52222));
	}
}